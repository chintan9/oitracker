'use strict';
angular.module('core').directive('oiTimesheetList',function(){
	return {
		restrict: 'E',
		templateUrl:'modules/users/views/oi-timesheetList.html'
	};
});