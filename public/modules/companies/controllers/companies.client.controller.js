'use strict';

// Companies controller
angular.module('companies').controller('CompaniesController', ['$scope', '$stateParams', '$location', 'Authentication', 'Companies', '$upload',
	function($scope, $stateParams, $location, Authentication, Companies, $upload ) {
		$scope.authentication = Authentication;

		// Create new Company
		$scope.create = function() {

			if($scope.action==='Edit'){
				return $scope.update();
			}

			// Create new Company object
			var company = new Companies ($scope.company);

			// Redirect after save
			company.$save(function(response) {
				$location.path('companies/' + response._id);
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Remove existing Company
		$scope.remove = function( company ) {
			if ( company ) { company.$remove();

				for (var i in $scope.companies ) {
					if ($scope.companies [i] === company ) {
						$scope.companies.splice(i, 1);
					}
				}
			} else {
				$scope.company.$remove(function() {
					$location.path('companies');
				});
			}
		};

		// Update existing Company
		$scope.update = function() {
			var company = $scope.company ;

			company.$update(function() {
				$location.path('companies/' + company._id);
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Find a list of Companies
		$scope.find = function() {
			$scope.companies = Companies.query();
		};

		// Find existing Company
		$scope.findOne = function() {
			$scope.action = 'New';
			if($stateParams.companyId){
				$scope.action = 'Edit';
				$scope.company = Companies.get({
					companyId: $stateParams.companyId
				});
			}
		};

		$scope.uploadLogo = function(){
			$upload.upload({
				url:'/companies/uploadLogo',
				data: {_id: $scope.company._id},
				file: $scope.company.file
			}).success(function(data) {
				console.log(arguments);
			});
		};

		$scope.onFileSelect = function(files){
			var file = files[0];
			if(file.type==='image/jpeg' || file.type === 'image/png' || file.type === 'image/gif'){
				$scope.company.file = file;
				$scope.fileError = '';
			} else {
				delete $scope.company.file;
				$scope.fileError = 'Please select valid image';
			}
		};

	}
]);